### Requirements
django 3.2
django-oauth-toolkit

### Get started
'''
# Put all the static files in place for direct serving via webserver
# Don't forget to make the static directory readable by your webserver
./manage.py collectstatic --noinput

#Make the "MEDIA_ROOT/uploads" directory readable by the webserver

# Create db
./manage.py migrate --run-syncdb

# Load the inital data into the db
./manage.py loaddata feuerwehr/fixtures/initial_data.yaml

# Run the app as ASGI
daphne feuerwehr.asgi:application
'''

### Auth
Register the app in oauth, call:
/o/applications/register

Generally follow the instructions at https://django-oauth-toolkit.readthedocs.io/en/latest/getting_started.html to get a token..

curl \
    -H "Authorization: Bearer jooqrnOrNa0BrNWlg68u9sl6SkdFZg" \
    -X GET http://localhost:8000/resource
