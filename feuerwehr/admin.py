from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User, Person, Role, Location, Alarm, Alarm_members, Dienst, Dienst_members

# Define an inline admin descriptor for Employee model
class PersonInline(admin.StackedInline):
    model = Person
    can_delete = False
    verbose_name_plural = 'Personen'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (PersonInline,)

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class AlarmInline(admin.StackedInline):
    model = Alarm


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    inlines = [
        AlarmInline,
    ]
    
admin.site.register(Role)
admin.site.register(Alarm)
admin.site.register(Alarm_members)
admin.site.register(Dienst)
admin.site.register(Dienst_members)
