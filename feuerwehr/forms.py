from django.forms import ModelForm
from feuerwehr.models import Alarm

class AlarmForm(ModelForm):
    class Meta:
        model = Alarm
        exclude = []
        #fields = ['name', 'title', 'birth_date']
