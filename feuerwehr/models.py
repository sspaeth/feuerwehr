from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


from datetime import datetime, timedelta
import logging
from pytz import timezone

# The timezone as configured in the django settings
cur_tz = timezone(settings.TIME_ZONE)

def user_get_absolute_url(self):
    return reverse('user-detail', kwargs={"username": self.username})
User.get_absolute_url = user_get_absolute_url

def user_get_name(self):
    same_f_name = User.objects.filter(first_name=self.first_name)
    if len(same_f_name) == 1 and self.first_name > "":
        return self.first_name
    if self.last_name > "":
        return f"{self.first_name} {self.last_name[0]}."
    return self.username
User.__str__ = user_get_name


DEPT = [
    (0, ""),
    (1, "Einsatzabteilung"),
    (2, "Ehrenabteilung"),
    (3, "Reserveabteilung"),
    (4, "JF"),
]


class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    dep  = models.SmallIntegerField(default=0)
    roles = models.ManyToManyField('Role', blank=True)

    def __str__(self):
        return str(self.user)

class Attendance(models.IntegerChoices):
    BESTÄTIGT = 1, _("Bestätigt")
    YES = 2, _("Zugesagt")
    NO = 3, _("Abgesagt")
    __empty__ = _("Unbekannt")


class Location(models.Model):
    lat = models.FloatField()
    lon = models.FloatField()
    # sanitized: all lowercase and str.->straße
    address = models.CharField(max_length=255, blank=True)
    # lat/lon get filled in in signals.py
    def __str__(self):
        return self.address

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['lat', 'lon'],
                                    name='unique_location')
            ]


class Alarm(models.Model):
    def autoinc_num(*args,**kwargs):
        """Provide the next free alarm number"""
        maxalarm = Alarm.objects.filter(year=datetime.now(tz=cur_tz).year).order_by('no').last()
        if not maxalarm:
            return 1
        else:
            return maxalarm.no + 1

    # inherited: comment, date, duration, location, participants
    key = models.CharField(max_length=50, help_text="Alarmstichwort")
    year = models.SmallIntegerField(default=datetime.now(tz=cur_tz).year,
                                    editable=False,
                                    help_text="Jahr des Alarms")
    no = models.SmallIntegerField(default=autoinc_num, editable=False,
                                  help_text="Laufende Alarmierungsnummer im Jahr")
    date = models.DateTimeField(default=datetime.now(tz=cur_tz))
    # HOw long did the event last, 00:00:00 and not status "aborted" implies ongoing
    duration = models.DurationField(default=timedelta(), help_text="Dauer (h:m:s)")
    # Adress, e.g. "Am Volksdorfer Markt 100"
    address = models.CharField(max_length=150, blank=True,
                               verbose_name="Adresse",
                               help_text="Straße & Hausnummer des Einsatzortes")
    # A geographical point/address, which can be used to center the map
    location = models.ForeignKey(Location, null=True, blank=True, editable=False,
                                 on_delete=models.SET_NULL)
    comment = models.CharField(max_length=255, blank=True)
    participants = models.ManyToManyField(
        User,
        blank=True,
        through='Alarm_members',
        through_fields=('event', 'user'),
    )
    # whether we went to the Alarm, responded=False implies we did not.
    responded = models.BooleanField(default=True)

    def is_ongoing(self):
        """whether the alarm is currently ongoing"""
        logging.error("duration",self.duration)
        return self.responded and self.duration == timedelta(seconds=0)

    def get_absolute_url(self):
        return reverse('alarm-detail', kwargs={'year': self.year, 'no': self.no})

    def __str__(self):
        return f"{self.key} #{self.no}"

    class Meta:
        get_latest_by = "date"
        unique_together = [['year', 'no']]
        ordering = ("-year", "-no")
        verbose_name_plural = _("Alarme")

class Dienst(models.Model):
    name = models.CharField(max_length=200, blank=True,
                            help_text="Titel des Dienstes")
    comment = models.CharField(max_length=255, blank=True)
    date = models.DateTimeField(default=datetime.now(tz=cur_tz))
    duration = models.DurationField(default="2:00:00", help_text="Dauer (h:m:s)")
    location = models.CharField(max_length=100, blank=True)
    participants = models.ManyToManyField(
        User,
        blank=True,
        through='Dienst_members',
        through_fields=('event', 'user'),
    )

    def get_absolute_url(self):
        return reverse('dienst-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name


class Role(models.Model):
    """Roles such as AGT, Maschinist, etc..."""
    name = models.CharField(max_length=100)
    icon = models.ImageField(upload_to="uploads/", max_length=255,
                              null=True, blank=True)

    def __str__(self):
        return self.name


class Alarm_members(models.Model):
    """ Through model connecting Events and invited/attending Users"""

    event = models.ForeignKey(Alarm, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    attendance = models.SmallIntegerField(choices=Attendance.choices,
                                          default=Attendance.YES,
                                          null=True, blank=True,
                                          help_text="Teilnahmestatus")
    role = models.ForeignKey(Role, null=True, blank=True, on_delete=models.CASCADE)
    attendance_reason = models.CharField(max_length=128, blank=True)

    def get_color(self):
        """Return color of the attendance"""
        return self.attendance.color
    
    def __str__(self):
        return f"{self.attendance} ({self.event}, {self.user.username})"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['event', 'user'],
                                    name='unique_alarm_participation')
            ]
        ordering = ("event", "attendance", "role")

class Dienst_members(models.Model):
    """ Through model connecting Events and invited/attending Users"""

    event = models.ForeignKey(Dienst, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    attendance = models.SmallIntegerField(choices=Attendance.choices,
                                  default=Attendance.__empty__,
                                  help_text="Teilnahmestatus")
    role = models.ForeignKey(Role, null=True, blank=True, on_delete=models.CASCADE)
    attendance_reason = models.CharField(max_length=128, blank=True)

    def get_color(self):
        """Return color of the attendance"""
        return self.attendance.color
    
    def __str__(self):
        return f"{self.attendance} ({self.event}, {self.user.username})"

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['event', 'user'],
                                    name='unique_dienst_participation')
            ]
        ordering = ("event", "attendance", "role")
