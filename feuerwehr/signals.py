import logging
import json

from urllib.request import urlopen
from urllib.parse import quote_plus

from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import Alarm, Location

def on_alarm_create_cb(sender, instance, raw, using, update_fields, **kwargs):
    """
    This is sent at the beginning of a model’s save() method.
    sender: should be <class 'feuerwehr.models.Alarm'>
    instance: The actual instance being saved.
    raw: A boolean; True if the model is saved exactly as presented (i.e. when loading a fixture). One should not query/modify other records in the database as the database might not be in a consistent state yet.
    using: The database alias being used. ?
1update_fields
    The set of fields to update as passed to Model.save(), or None if update_fields wasn’t passed to save().
    """
    if raw: return
    if instance.location is not None: return #(already have a location)
    address = instance.address.lower().replace("str.", "straße")
    try:
        loc, created = Location.objects.get_or_create(address=address)
        instance.location = loc
    except Location.DoesNotExist as e:
        logging.warn("Not saving Location, due to %s", str(e))

def on_location_create_cb(sender, instance, raw, using, update_fields, **kwargs):
    if raw: return
    #TODO make async to not lose seconds when an alarm is created! (But it is pretty fast right now)
    #https://nominatim.openstreetmap.org/search?addressdetails=0&street=Farmsener+Landstr+81&layer=address&city=Hamburg&format=geojson&limit=1
    # only look up lat/lon if they are not filled in already (e.g. through manual creation)
    if not (instance.lat is None and  instance is None): return
    logging.debug("Looking up address", instance.address)
    address_safe = quote_plus(instance.address)
    res = None
    with urlopen(f"https://nominatim.openstreetmap.org/search?&layer=address&city=Hamburg&format=geojson&limit=1&addressdetails=0&street={address_safe}", data=None, timeout=5, context=None) as f:
        res = json.load(f)
    if not res.get("type", None) == 'FeatureCollection':
        raise Location.DoesNotExist("weird Nominatim response")
    res = res.get("features", None)
    if not res:
        raise Location.DoesNotExist("Nominatim response contains no features (Loc not found)")
    geo = res[0].get("geometry", None)
    if not geo:
        raise Location.DoesNotExist("Nominatim response contains no geometry")
    # geometry': {'type': 'Point', 'coordinates': [10.157132201091883, 53.6413276]}}]}
    logging.warn(geo)
    #if not geo.get("type", "Point", None):
    #    raise Location.DoesNotExist("Nominatim response is not a Point")
    (instance.lon, instance.lat) = geo.get("coordinates")

