const copy =
  "&copy; <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a>";
const url =
  "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
const layer = L.tileLayer(url, {
  attribution: copy,
});
  const map = L.map("map", {
  layers: [layer],
});
/*map.setView([53.6513, 10.1726], 16)*/
map.fitWorld();
