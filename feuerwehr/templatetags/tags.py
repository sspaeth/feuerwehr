from django import template
from feuerwehr.models import Alarm_members
register = template.Library()

@register.filter
def get_membership(alarm, user):
    """Get a membership from a set, that belongs to a specific user.

    use as alarm|get_membership:user"""
    return Alarm_members.objects.get(event=alarm, user=user)
