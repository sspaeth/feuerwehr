"""feuerwehr URL Configuration
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from . import views
from .signals import on_alarm_create_cb, on_location_create_cb

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.HomepageView.as_view(), name="home"),
    path('alarm/', views.AlarmListView.as_view(), name="alarm-list"),
    path('alarm/create', views.AlarmCreateView.as_view(), name="alarm-create"),
    path('alarm/<int:year>/<int:no>/', views.AlarmDetailView.as_view(), name="alarm-detail"),
    path('alarm/<int:no>/', views.AlarmDetailView.as_view(), name="alarm-detail"),
    path('alarm/<int:year>/<int:no>/edit', views.AlarmUpdateView.as_view(), name="alarm-edit"),
    path('u/', views.UserListView.as_view(), name="user-list"),
    path('u/<str:username>/', views.UserDetailView.as_view(), name="user-detail"),
    path('map/', include('maps.urls')),
    path('accounts/login/', auth_views.LoginView.as_view(), name="login"),
    path('accounts/password_reset/', auth_views.PasswordResetView.as_view(), name="password_reset"),
    path('accounts/password_reset_done/', auth_views.PasswordResetConfirmView.as_view(),
         name="password_reset_confirm"),
    #path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]
# only in debug mode provide static files: + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

from django.db.models import signals
from .models import Alarm, Location
# registering signals with the model's string label
signals.pre_save.connect(on_alarm_create_cb, sender=Alarm)
signals.pre_save.connect(on_location_create_cb, sender=Location)
