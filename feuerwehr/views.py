from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count, OuterRef, Subquery
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .forms import AlarmForm
from .models import Alarm, User

class HomepageView(TemplateView):
    template_name = "home.html"

class AlarmListView(LoginRequiredMixin,ListView):
    model = Alarm

class AlarmDetailView(LoginRequiredMixin,DetailView):
    model = Alarm

    def get_object(self, queryset=None):
        no = self.kwargs['no']
        year = self.kwargs.get('year', 2024)
        return self.model.objects.get(no=no,year=year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['participants'] = self.object.alarm_members_set.all()
        return context


class AlarmCreateView(LoginRequiredMixin,CreateView):
    model = Alarm
    form_class = AlarmForm
    #fields = ['name']


class AlarmUpdateView(LoginRequiredMixin,UpdateView):
    model = Alarm

class UserListView(LoginRequiredMixin,ListView):
    model = User
    ordering = ["first_name", "last_name", "username"]


class UserDetailView(LoginRequiredMixin,DetailView):
    model = User
    slug_field = "username"
    slug_url_kwarg = "username"
    #def get_object(self, queryset=None):
    #    no = self.kwargs['no']
    #    year = self.kwargs.get('year', 2024)
    #    return self.model.objects.get(no=no,year=year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # all alarms that have attendance "bestätigt" or "zugesagt"
        alarme = Alarm.objects.filter(participants=self.object, alarm_members__attendance__lte=2)
        context['alarme'] = alarme
        context['num_alarme'] = alarme.count()
        context['tot_alarme'] = Alarm.objects.count()
        return context
