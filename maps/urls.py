""" (comments)
"""
from django.urls import path
from . import views

urlpatterns = [
    path(r'', views.MarkersMapView.as_view(), name="default-map"),
]
