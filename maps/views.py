from django.views.generic.base import (
    TemplateView,
)
def default_map(request):
    return render(request, 'default.html', {})

class MarkersMapView(TemplateView):
    template_name = "map.html"
